const slideList = [{
    img: "images/img1.jpg",
    text: 'Pierwszy tekst'
    },
    {
    img: "images/img2.jpg",
    text: 'Drugi tekst'
    },
    {
    img: "images/img3.jpg",
    text: 'Trzeci tekst'
    }];

    const image = document.querySelector('img.slider');
    const h1 = document.querySelector('h1.slider');
   // Interfejs
    const time = 2000;
    let active = 0;
    const dots = [...document.querySelectorAll('.dots span')]



    const changeDots = () => {
        const activeDot = dots.findIndex(dot => dot.classList.contains('active'))
        dots[activeDot].classList.remove('active');
        dots[active].classList.add('active');

    }
    // Implementacje
    
    const changeSlide = () => {
        active++;
        if (active === slideList.length) {
            active = 0;
        }
    image.src = slideList[active].img;
    h1.textContent = slideList[active].text;
    changeDots()

    }
    let indexInterval = (changeSlide, time);

    const keyChangeSlide = (e) => {
        if(e.keyCode == 37 || e.keyCode == 39){
            e.keyCode == 37 ? active-- : active++;
            clearInterval(indexInterval)
            if(active === slideList.length) {
                active = 0
            } else if (active < 0) {
                active = slideList.length -1;
            }
            image.src = slideList[active].img;
            h1.textContent = slideList[active].text;
            changeDots()
            indexInterval = setInterval(changeSlide, time);
            console.log(active);
            }
        }

    


window.addEventListener('keydown', keyChangeSlide)